const nock = require('nock');

const CrawlyCrawler = require('../crawly-crawler');
const testURL = 'http://applicant-test.us-east-1.elasticbeanstalk.com/';

describe('getFormTokenAndCookie() tests', () => {
  let crawler;
  beforeEach(() => {
    crawler = new CrawlyCrawler(testURL);
    nock(testURL)
      .get('/')
      .reply(200, '<input id="token" value="123456">', {'set-cookie' : [ "cookies" ]});
  });

  it('Should return correctly cookie from Headers and token value from DOM', async () => {
    const response = await crawler.getFormTokenAndCookie();
    expect(response).toEqual({"cookie": "cookies", "token": "123456"} );
  });
});
