const { URLSearchParams } = require('url');
const fetch = require('node-fetch');
const DOMParser = require('dom-parser');
const decryptToken = require('./decrypt-token');

class CrawlyCrawler {
  constructor(requestURL) {
    this.parser = new DOMParser();
    this.requestUrl = requestURL;
  }

  parseDOMFromText(text) {
    return this.parser.parseFromString(text, 'text/html');
  }

  async getFormTokenAndCookie() {
    const httpResponse = await fetch(this.requestUrl);
    const responseText = await httpResponse.text();
    const pageDOM = this.parseDOMFromText(responseText);
    return {
      token: decryptToken(pageDOM.getElementById('token').getAttribute('value')),
      cookie: httpResponse.headers.get('set-cookie')
    };
  }

  async getNumberResponse(token, cookie) {
    const params = new URLSearchParams();
    params.append('token', token);
    const httpResponse = await fetch(this.requestUrl, {
      method: 'POST',
      body: params,
      headers: {
        'Referer': this.requestUrl,
        'Cookie': cookie
      }
    });
    const responseText = await httpResponse.text();
    const pageDOM = this.parseDOMFromText(responseText);
    return pageDOM.getElementById('answer').textContent;
  }

  async answer() {
    const response = await this.getFormTokenAndCookie();
    return await this.getNumberResponse(response.token, response.cookie);
  }
}

module.exports = CrawlyCrawler;
