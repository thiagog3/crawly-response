const CrawlyCrawler = require('./crawly-crawler');

(async () => {
  const crawlerUrl = process.env.URL || 'http://applicant-test.us-east-1.elasticbeanstalk.com/';
  const crawler = new CrawlyCrawler(crawlerUrl);
  console.log(await crawler.answer());
})();
